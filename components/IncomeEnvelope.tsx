import { useState, useEffect } from "react";
import Link from 'next/link';

type IncomeEnvelopeProps = {
  envelopeName: string;
  income: number;
  id: number;
  origIncome: number;
};

const IncomeEnvelope = (props: IncomeEnvelopeProps) => {
  const [editMode, setEditMode] = useState(false);
  const [editedEnvelopeName, setEditedEnvelopeName] = useState(props.envelopeName);
  const [editedIncome, setEditedIncome] = useState(props.income);
  const [isDeleted, setIsDeleted] = useState(false);
  const [totalExpenses, setTotalExpenses] = useState(0);
  const [progressPercentage, setProgressPercentage] = useState(0);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const storedData = JSON.parse(localStorage.getItem('financialData') || '{}');
        const envelopeData = storedData.envelopes[props.id];
        if (!envelopeData) {
          setIsDeleted(true);
        }

        // Calculate total expenses for the current envelope
        const expensesData = JSON.parse(localStorage.getItem('expensesData') || '{}');
        const expensesForEnvelope = expensesData.expenses?.filter((expense: any) => expense.envelopeId === props.id);
        const totalExpenseAmount = expensesForEnvelope?.reduce((sum: number, expense: any) => sum + expense.expenseAmount, 0);
        setTotalExpenses(totalExpenseAmount || 0);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchData();
  }, [props.id]);

  useEffect(() => {
    const remainingBalance = typeof props.origIncome === 'number' && typeof totalExpenses === 'number'
      ? props.origIncome - totalExpenses
      : 0;

    const percentage = (remainingBalance / props.origIncome) * 100;
    setProgressPercentage(percentage || 0);
  }, [props.origIncome, totalExpenses]);

  const currentBalance = typeof props.origIncome === 'number' && typeof totalExpenses === 'number'
    ? props.origIncome - totalExpenses
    : 0;

  const handleEdit = () => {
    setEditMode(true);
  };

  const isLowBalance = progressPercentage <= 10;

  const handleSaveEdit = () => {
    // Save the edited data to localStorage
    const updatedData = JSON.parse(localStorage.getItem('financialData') || '{}');

    // Find the index of the envelope with the matching id
    const envelopeIndex = updatedData.envelopes.findIndex((envelope: any) => envelope.id === props.id);

    if (envelopeIndex !== -1) {
      // Update the envelope data
      updatedData.envelopes[envelopeIndex] = {
        ...updatedData.envelopes[envelopeIndex],
        envelopeName: editedEnvelopeName,
        income: editedIncome,
        origIncome: editedIncome,
      };

      // Save the updated data to localStorage
      localStorage.setItem('financialData', JSON.stringify(updatedData));

      // Update the state to reflect changes
      setEditMode(false);
      setEditedEnvelopeName(editedEnvelopeName);
      setEditedIncome(editedIncome);
    }
  };

  const handleDelete = async () => {
    const confirmDelete = window.confirm('Are you sure you want to delete this envelope?');
    if (confirmDelete) {
      // Implement deletion logic for both income and associated expenses
      const storedData = JSON.parse(localStorage.getItem('financialData') || '{}');
      const updatedData = {
        ...storedData,
        envelopes: storedData.envelopes?.filter((envelope: any) => envelope.id !== props.id),
      };

      // Update the state to reflect changes
      setEditedEnvelopeName('');
      setEditedIncome(0);
      setEditMode(false);
      setIsDeleted(true);

      localStorage.setItem('financialData', JSON.stringify(updatedData));

      // Delay the deletion of associated expenses after state update
      await new Promise(resolve => setTimeout(resolve, 0));

      // Delete associated expenses
      const storedExpenseData = JSON.parse(localStorage.getItem('expensesData') || '{}');
      const updatedExpenseData = {
        ...storedExpenseData,
        expenses: storedExpenseData.expenses?.filter((expense: any) => expense.envelopeId !== props.id),
      };

      localStorage.setItem('expensesData', JSON.stringify(updatedExpenseData));
      location.reload();
    }
  };

  const formatMonetary = (origNum: number) => {
    const orgNum = origNum;
    const formattedNum = orgNum?.toLocaleString();
    return formattedNum;
  };

  return (
    <div className="m-2 p-4 bg-gray-700 rounded-md shadow-md">
      {editMode ? (
        <div>
          <label className="block mb-2 text-white">
            Envelope Name:
            <input
              type="text"
              value={editedEnvelopeName}
              onChange={(e) => setEditedEnvelopeName(e.target.value)}
              className="border p-2 rounded-md w-full text-black"
            />
          </label>
          <label className="block mb-2 text-white">
            Income:
            <input
              type="number"
              value={editedIncome}
              onChange={(e) => setEditedIncome(parseFloat(e.target.value) || 0)}
              className="border p-2 rounded-md w-full text-black"
            />
          </label>
          <div className="flex justify-between">
            <button className="bg-gray-800 hover:bg-gray-900 text-white px-4 py-2 rounded-md mr-2" onClick={handleSaveEdit}>
              Save
            </button>
            <button className="bg-gray-500 hover:bg-gray-400 text-white px-4 py-2 rounded-md" onClick={() => setEditMode(false)}>
              Cancel
            </button>
          </div>
        </div>
      ) : (
        <div>
          <Link href={`/expense/${props.id}`}>
            <div>
              <h2 className="text-xl text-white font-bold mb-2">{editedEnvelopeName}</h2>
              <p className="text-amber-500">Initial Income: {formatMonetary(props.origIncome)}</p>
              <p className="text-lg text-white mb-2">Current Income: <strong>
                {formatMonetary(currentBalance)}
              </strong>
              </p>
              <div className={`bg-gray-500 h-4 rounded-md mb-2`}>
                <div
                  className={`h-full rounded-md ${isLowBalance ? 'bg-red-500' : 'bg-amber-500'}`}
                  style={{ width: `${progressPercentage}%` }}
                ></div>
              </div>
            </div>
          </Link>

          <div className="flex gap-2 justify-center w-full">
            <button
              onClick={handleEdit}
              className="bg-gray-800 hover:bg-gray-900 text-white w-5/12 font-bold py-2 px-4 rounded"
            >
              📝
            </button>
            <button className="bg-gray-500 hover:bg-gray-400 text-white px-4 py-2 rounded-md" onClick={handleDelete}>
              🗑️
            </button>
          </div>
        </div>
      )}
    </div>
  );
};

export default IncomeEnvelope;
