"use client";
import { useState, useEffect } from 'react';
import IncomeEnvelope from './IncomeEnvelope';

type Props = {}

const ExpenseCardWrapper = () => {
  const [envelopes, setEnvelopes] = useState<any[]>([]);

  useEffect(() => {
    if (typeof window !== 'undefined') {
      const storedData = localStorage.getItem('financialData');
      if (storedData) {
        const parsedData = JSON.parse(storedData);
        setEnvelopes(parsedData.envelopes || []);
      }
    }
  }, []);

  return (
    <div className="flex flex-wrap items-stretch justify-center w-full">
      {envelopes?.map((envelope: any) => (
        <IncomeEnvelope
          key={envelope.id} // Use the unique ID as the key
          envelopeName={envelope.envelopeName}
          income={envelope.income}
          id={envelope.id}
          origIncome={envelope.origIncome}
        />
      ))}
    </div>
  );
};


export default ExpenseCardWrapper;
