"use client";

import React, { useState } from 'react';
import { v4 as uuidv4 } from 'uuid';

const IncomeForm = () => {
  const [income, setIncome] = useState('');
  const [envelopName, setEnvelopeName] = useState('');
  const [newEnvelop, setNewEnvelop] = useState(false);

  const handleNewEnvelop = () => {
    setNewEnvelop(!newEnvelop);
    setEnvelopeName('');
    setIncome('');
  };

  const handleIncomeChange = (e: any) => {
    setIncome(e.target.value);
  };

  const handleEnvelopChange = (e: any) => {
    setEnvelopeName(e.target.value);
  };

  interface FinancialData {
    existingData: number | null;
    envelopes: [];
  }

  const handleIncomeSubmit = (e: any) => {
    e.preventDefault();

    let newData = {};

    const storedData = localStorage.getItem('financialData');
    if (storedData !== null) {
      const existingData: FinancialData = JSON.parse(storedData);
      newData = {
        ...existingData,
        envelopes: [
          ...(existingData.envelopes || []),
          { id: uuidv4(), envelopeName: envelopName, income: parseFloat(income) || 0, origIncome: parseFloat(income) || 0 },
        ],
      };
    } else {
      newData = { envelopes: [{ id: uuidv4(), envelopeName: envelopName, income: parseFloat(income) || 0 }] };
    }

    localStorage.setItem('financialData', JSON.stringify(newData));
    location.reload();
    setNewEnvelop(false);
    setEnvelopeName('');
    setIncome('');
  };

  return (
    <>
      {!newEnvelop ? (
        <button
          className='w-6/12 md:w-1/4 bg-amber-500 text-white rounded-md hover:bg-amber-600 transition-all ease-in-out p-3'
          onClick={handleNewEnvelop}>
          New Envelope
        </button>
      ) : (
        <div className="fixed inset-0 flex items-center justify-center z-50">
          <div className="modal-overlay fixed inset-0 bg-black opacity-50"></div>

          <div className="modal-container bg-gray-700 w-11/12 md:max-w-md mx-auto rounded shadow-lg z-50 overflow-y-auto">
            {/* Your form goes here */}
            <form onSubmit={handleIncomeSubmit} className="p-4">
              <div className="mb-4 text-center">
                <label className='text-white text-left flex'>Income Amount</label>
                <input
                  type="number"
                  value={income}
                  onChange={handleIncomeChange}
                  className="w-full px-3 py-2 border rounded text-black border-gray-300"
                  required
                />
                <label className='text-white text-left flex  mt-3'>Envelope Name</label>
                <input
                  type="text"
                  value={envelopName}
                  onChange={handleEnvelopChange}
                  className="w-full px-3 py-2 border rounded text-black border-gray-300"
                  required
                />
              </div>
              <div className='flex flex-wrap gap-2 justify-center w-full'>
                <button
                  type="submit"
                  className="bg-gray-800 hover:bg-gray-900 text-white w-5/12 font-bold py-2 px-4 rounded"
                >
                  Create
                </button>
                <button
                  onClick={() => {
                    setNewEnvelop(false);
                  }}
                  className="bg-gray-500 hover:bg-gray-400 text-white font-bold w-5/12 py-2 px-4 rounded"
                >
                  Cancel
                </button>
              </div>
            </form>
          </div>
        </div>
      )}
    </>
  );
};

export default IncomeForm;
