"use client";

import React, { useEffect, useState } from 'react'
import ExpenseCard from './ExpenseCard';
import Link from 'next/link';
import { v4 as uuidv4 } from 'uuid';

type Props = {
  id: number;
}
interface Expense {
  id: string;
  envelopeId: string;
  expenseName: string;
  expenseAmount: number;
  origIncome: number;
}

const ExpenseEnvelope = (props: Props) => {
  const [totalBudget, setTotalBudget] = useState<number>(0);
  const [activeEnvelope, setActiveEnvelope] = useState<any>();
  const [expenseItems, setExpenseItems] = useState<any[]>();
  const [envelopeName, setEnvelopeName] = useState<any[]>();
  const [addExpenseModal, setAddExpenseModal] = useState(false);
  const [activeExpense, setActiveExpense] = useState<Expense | null>(null);
  const [totalExpenses, setTotalExpenses] = useState(0)

  const [expenseAmount, setExpenseAmount] = useState<string>('');
  const [expenseName, setExpenseName] = useState<string>('');

  useEffect(() => {
    if (typeof window !== 'undefined') {
      const storedData = localStorage.getItem('financialData');
      let initialBudget = 0;
      let parsedData;
      if (storedData) {
        parsedData = JSON.parse(storedData);
      }
      if (parsedData) {
        const activeEnvelopeInternal = parsedData?.envelopes.find((envelope: any) => envelope.id === props?.id);

        // Update origIncome for the activeEnvelopeInternal
        const updatedEnvelope = {
          ...activeEnvelopeInternal,
          origIncome: activeEnvelopeInternal.income,
        };

        // Update the financialData with the modified activeEnvelopeInternal
        const updatedFinancialData = {
          ...parsedData,
          envelopes: parsedData.envelopes?.map((envelope: any) =>
            envelope.id === props?.id ? updatedEnvelope : envelope
          ),
        };

        // Save the updated financialData to localStorage
        localStorage.setItem('financialData', JSON.stringify(updatedFinancialData));

        setActiveEnvelope(updatedEnvelope);

        initialBudget = activeEnvelopeInternal?.income;
        setEnvelopeName(activeEnvelopeInternal?.envelopeName);

        setTotalBudget(initialBudget);
      }

      const storedExpenseData = localStorage.getItem('expensesData');
      let parsedExpenseData;
      if (storedExpenseData) {
        parsedExpenseData = JSON.parse(storedExpenseData);
      }
      if (parsedExpenseData && initialBudget) {
        const filteredExpenses = parsedExpenseData?.expenses?.filter(
          (expense: any) => expense.envelopeId === props.id
        );
        setExpenseItems(filteredExpenses);

        let total = 0;
        filteredExpenses?.forEach((expense: any) => {
          total += Number(expense.expenseAmount);
        });

        setTotalExpenses(total);

        setTotalBudget(initialBudget - total);
      }
    }
  }, [props.id]);


  const handleExpenseAmountChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setExpenseAmount(e.target.value);
  };

  const handleExpenseNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setExpenseName(e.target.value);
  };

  const handleNewExpense = () => {
    setExpenseAmount('');
    setExpenseName('');
    setAddExpenseModal(!addExpenseModal);
    setActiveExpense(null);
  };

  const handleExpenseSubmit = (e: React.FormEvent) => {
    e.preventDefault();

    let newExpense = {
      id: activeExpense ? activeExpense.id : uuidv4(),
      envelopeId: props.id,
      expenseName: expenseName,
      expenseAmount: parseFloat(expenseAmount) || 0,
      origIncome: 0,
    };

    const updatedTotalBudget = totalBudget - newExpense.expenseAmount;

    newExpense = {
      ...newExpense,
      origIncome: updatedTotalBudget,
    };
    setTotalBudget(updatedTotalBudget);

    let newData = {};

    const storedExpensesData = localStorage.getItem('expensesData');
    if (storedExpensesData !== null) {
      const existingExpenseData = JSON?.parse(storedExpensesData);

      if (activeExpense) {
        newData = {
          ...existingExpenseData,
          expenses: existingExpenseData.expenses?.map((expense: any) =>
            expense.id === activeExpense.id ? newExpense : expense
          ),
        };
      } else {
        newData = {
          ...existingExpenseData,
          expenses: [...(existingExpenseData.expenses || []), newExpense],
        };
      }
    } else {
      newData = {
        expenses: [newExpense],
      };
    }

    localStorage.setItem('expensesData', JSON.stringify(newData));

    const updatedExpenseItems = activeExpense
      ? expenseItems?.map((expense) => (expense.id === activeExpense.id ? newExpense : expense))
      : [...(expenseItems || []), newExpense];

    setExpenseItems(updatedExpenseItems);

    setAddExpenseModal(false);
    setExpenseName('');
    setExpenseAmount('');
    location.reload();
  };


  const handleDeleteExpense = (id: string) => {
    const storedExpensesData = localStorage.getItem('expensesData');
    if (storedExpensesData !== null) {
      const existingExpenseData = JSON.parse(storedExpensesData);
      const updatedExpenses = existingExpenseData.expenses?.filter((expense: any) => expense.id !== id);
      const newData = {
        ...existingExpenseData,
        expenses: updatedExpenses,
      };
      localStorage.setItem('expensesData', JSON.stringify(newData));
      location.reload();
    }
  };


  const formatMonetary = (origNum: number) => {
    const orgNum = origNum;
    const formattedNum = orgNum?.toLocaleString();
    return formattedNum;
  }

  return (
    <div style={{ height: '100vh' }} className="flex flex-col px-5">
      <Link href="/" className="px-3 py-2 absolute bg-gray-900 rounded-md text-white hover:bg-gray-800">
        🏠 Home
      </Link>
      <div className="py-10 flex flex-wrap items-stretch justify-center w-full text-center">
        <div className="bg-gray-800 text-white py-2 w-full md:w-1/4 rounded-md">
          <strong>{envelopeName}</strong>
          <h2 className="font-bold text-2xl text-amber-500">{formatMonetary(totalBudget)}</h2>
          <span className='text-red-500'>{formatMonetary(totalExpenses)}</span>
        </div>
      </div>
      <div className="flex flex-wrap items-stretch justify-center w-full">
        {expenseItems?.map((expense: any) => (
          <ExpenseCard
            setExpenseAmount={setExpenseAmount}
            setExpenseName={setExpenseName}
            key={expense.id}
            expenseName={expense.expenseName}
            expenseAmount={expense.expenseAmount}
            id={expense.id}
            handleClick={() => {
              setActiveExpense(expense);
              setAddExpenseModal(!addExpenseModal);
              setExpenseAmount(expense.expenseAmount.toString());
              setExpenseName(expense.expenseName);
            }}
            onDelete={handleDeleteExpense}
          />
        ))}
      </div>
      {!addExpenseModal ? (
        <div className="flex justify-center">
          <button
            className="w-full md:w-1/4 bg-amber-500 text-white hover:bg-amber-600 transition-all ease-in-out rounded-md mt-2 py-3"
            onClick={handleNewExpense}
          >
            New Expense
          </button>
        </div>
      ) : (
        <div className="fixed inset-0 flex items-center justify-center z-50">
          <div className="modal-overlay fixed inset-0 bg-black opacity-50"></div>
          <div className="modal-container bg-gray-700 w-11/12 md:max-w-md mx-auto rounded shadow-lg z-50 overflow-y-auto">
            <form onSubmit={handleExpenseSubmit} className="p-4">
              <div className="mb-4 text-center">
                <label className='flex text-left text-white'>Expense Amount</label>
                <input
                  type="number"
                  value={expenseAmount}
                  onChange={handleExpenseAmountChange}
                  className="w-full px-3 py-2 border rounded text-black border-gray-300"
                />
                <label className='flex text-left mt-3 text-white'>Expense Name</label>
                <input
                  type="text"
                  value={expenseName}
                  onChange={handleExpenseNameChange}
                  className="w-full px-3 py-2 border rounded text-black border-gray-300"
                />
              </div>
              <div className="flex flex-wrap gap-2 justify-center w-full">
                <button
                  type="submit"
                  className="bg-gray-800 hover:bg-gray-900 text-white w-5/12 font-bold py-2 px-4 rounded"
                >
                  Submit
                </button>
                <button
                  onClick={() => {
                    setAddExpenseModal(false);
                  }}
                  className="bg-gray-500 hover:bg-gray-400 text-white font-bold w-5/12 py-2 px-4 rounded"
                >
                  Cancel
                </button>
              </div>
            </form>
          </div>
        </div>
      )}
    </div>
  );
};

export default ExpenseEnvelope;
