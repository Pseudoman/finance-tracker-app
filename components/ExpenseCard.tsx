"use client"

import React from "react";

type ExpenseCardProps = {
  expenseName: string;
  expenseAmount: number;
  id: string;
  handleClick: () => void;
  setExpenseAmount: (amount: string) => void;
  setExpenseName: (name: string) => void;
  onDelete: (id: string) => void;
};

const ExpenseCard: React.FC<ExpenseCardProps> = (props) => {
  const [deleteModalOpen, setDeleteModalOpen] = React.useState(false);

  const handleEdit = () => {
    props.handleClick();
    props.setExpenseAmount(props.expenseAmount.toString());
    props.setExpenseName(props.expenseName);
  };

  const handleDelete = () => {
    setDeleteModalOpen(true);
  };

  const confirmDelete = () => {
    props.onDelete(props.id);
    setDeleteModalOpen(false);
  };

  const cancelDelete = () => {
    setDeleteModalOpen(false);
  };

  const formatMonetary = (origNum: number) => {
    const orgNum = origNum;
    const formattedNum = orgNum?.toLocaleString();
    return formattedNum;
  }

  return (
    <div className="justify-between flex flex-row my-2 md:mx-2 lg:m-2 w-full md:w-2/6 lg:w-1/4 gap-4 bg-gray-700 text-white p-4 self-auto shadow rounded">
      <div>
        <h2 className="text-xl font-bold mb-2 break-all">{props.expenseName}</h2>
        <h3 className="text-lg">{formatMonetary(props.expenseAmount)}</h3>
      </div>
      <div className="flex flex-col justify-center">
        <button onClick={handleEdit}>📝</button>
        <button onClick={handleDelete}>🗑️</button>
      </div>

      {/* Delete Confirmation Modal */}
      {deleteModalOpen && (
        <div className="fixed inset-0 flex items-center justify-center z-50">
          <div className="modal-overlay fixed inset-0 bg-black opacity-50"></div>
          <div className="modal-container text-white bg-gray-700 w-6/12 flex flex-col text-center py-5 justify-evenly md:max-w-md mx-auto rounded shadow-lg z-50 overflow-y-auto">
            <h3>Are you sure you want to delete this expense?</h3>
            <div className="mt-2">
              <button onClick={confirmDelete} className="px-4 py-2 bg-gray-800 hover:bg-gray-900 text-white rounded-md mr-2">
                Confirm
              </button>
              <button onClick={cancelDelete} className="px-4 py-2 bg-gray-500 hover:bg-gray-600 text-white rounded-md">
                Cancel
              </button>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default ExpenseCard;
