import ExpenseEnvelope from "@/components/ExpenseEnvelope";

export default function Page({ params }: { params: { id: number } }) {
  return (
    <ExpenseEnvelope id={params?.id} />
  )
}
