import ExpenseCardWrapper from '@/components/ExpenseCardWrapper'
import IncomeEnvelope from '@/components/IncomeEnvelope'
import IncomeForm from '@/components/IncomeForm'
import Link from 'next/link';
import { FaGithub, FaGitlab, FaLinkedin } from "react-icons/fa";

export default function Home() {
  const currentYear = new Date().getFullYear();
  return (
    <>
      <main>
        <div className="bg-gray-800 text-white py-2 rounded-md text-center flex justify-center w-full">
          <strong>Tracky Clone</strong>
        </div>
        <div
          className="flex min-h-screen flex-col items-center justify-center gap-3 p-5 md:p-20">
          <div className='flex flex-wrap items-stretch justify-center w-full text-center'>
          </div>
          <ExpenseCardWrapper />
          <IncomeForm />
        </div>
      </main>
      <footer className="text-center text-gray-500">
        <ul className='flex flex-col justify-center my-3 gap-3 text-center items-center md:flex-row text-gray-500'>
          <li className='flex flex-row gap-1'>
            <FaGitlab className='my-auto' />
            <Link href="https://gitlab.com/Pseudoman" target='_blank' className='flex flex-row my-auto gap-1'>
              GitLab
            </Link>
          </li>
          <li className='flex flex-row gap-1'>
            <Link href='https://github.com/Pseudoman21' target='_blank' className='flex flex-row my-auto gap-1'>
              <FaGithub className='my-auto' />
              GitHub
            </Link>
          </li>
          <li className='flex flex-row gap-1'>
            <Link href='https://www.linkedin.com/in/john-paul-batusan-8361b617a' target='_blank' className='flex gap-1 flex-row my-auto'>
              <FaLinkedin className='my-auto' />
              LinkedIn
            </Link>
          </li>
        </ul>
        <p>
          Made with love by Pseudoman21©{currentYear}
        </p>
      </footer>
    </>
  )
}
